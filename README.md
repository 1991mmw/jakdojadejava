Bicycle Rental Application made in Spring Boot

http://localhost:8080

* [/station/getLightweightBicycleStations],methods=[GET],produces=[application/json]
* [/station/update],methods=[PUT],consumes=[application/json],produces=[application/json]
* [/station/add],methods=[POST]
* [/station/remove],methods=[DELETE]
* [/station/getByPublicId],methods=[GET],produces=[application/json]
* [/bicycle/dock/{bicyclePublicId}/{stationPublicId}],methods=[PUT],consumes=[application/json]
* [/bicycle/getByPublicId],methods=[GET],produces=[application/json]
* [/bicycle/rent/{bicyclePublicId}/{stationPublicId}],methods=[PUT],consumes=[application/json]