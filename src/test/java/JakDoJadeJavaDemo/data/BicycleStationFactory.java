package JakDoJadeJavaDemo.data;

import JakDoJadeJavaDemo.model.Station;

import java.util.UUID;

public class BicycleStationFactory {

    public static Station createDefaultEntity() {
        return new Station(UUID.randomUUID().toString(), "Poznan Glowny", "12", "0", "12", "0", null);
    }
}
