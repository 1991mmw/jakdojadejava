package JakDoJadeJavaDemo;

import JakDoJadeJavaDemo.data.BicycleStationFactory;
import JakDoJadeJavaDemo.model.Station;
import JakDoJadeJavaDemo.service.impl.BicycleStationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BicycleStationServiceApplicationTests {

	private static final Logger LOGGER = Logger.getLogger(BicycleStationServiceApplicationTests.class.getName());

	@Test
	public void contextLoads() {
	}

	@Autowired
	private BicycleStationServiceImpl testedService;

	@Test
	public void shouldCreateBicycleStation() {
		//given

		//when
		testedService.add(BicycleStationFactory.createDefaultEntity());
		//then

	}

	@Test
	public void shouldGetBicycleStation() {
		//given
		//create station and remove if exists
		Station defaultEntity = BicycleStationFactory.createDefaultEntity();
		testedService.add(defaultEntity);

		//when
		//search for created station
		Station byPublicId = testedService.getByPublicId(defaultEntity.getPublicId());
		LOGGER.info(byPublicId.toString());

		//then
		assertEquals(byPublicId, defaultEntity);
	}

	@Test
	public void shouldRemoveBicycleStation() {
	    //given
        Station entity = BicycleStationFactory.createDefaultEntity();
        testedService.add(entity);
        //when
        testedService.remove(entity.getPublicId());
        //given
        assertNull(testedService.getByPublicId(entity.getPublicId()));
    }

}
