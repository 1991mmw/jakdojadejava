package JakDoJadeJavaDemo.service.assembler;

import JakDoJadeJavaDemo.model.LightweightStation;
import JakDoJadeJavaDemo.model.Station;
import org.springframework.stereotype.Component;

@Component
public class LightweightStationAssembler {

    public LightweightStation assembleLightweightStation(Station station) {
        LightweightStation lightweightStation = new LightweightStation();

        lightweightStation.setAvailableBicycles(station.getTotalBicycleUnits());
        lightweightStation.setName(station.getName());
        lightweightStation.setOccupiedRacks(station.getOccupiedRacks());
        lightweightStation.setVacancies(station.getTotalBicycleRacks());
        return lightweightStation;
    }
}
