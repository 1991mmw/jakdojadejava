package JakDoJadeJavaDemo.service;

import JakDoJadeJavaDemo.model.Bicycle;

public interface BicycleService {

    void dockBicycle(String stationPublicId, String bicyclePublicId);

    void rentBicycle(String stationPublicId, String bicyclePublicId);

    Bicycle getByPublicId(String publicId);
}
