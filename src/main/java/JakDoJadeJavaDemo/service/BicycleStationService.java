package JakDoJadeJavaDemo.service;

import JakDoJadeJavaDemo.model.Bicycle;
import JakDoJadeJavaDemo.model.LightweightStation;
import JakDoJadeJavaDemo.model.Station;

import java.util.List;

public interface BicycleStationService {

    void add(Station station);

    void remove(String publicId);

    void update(Station station);

    Station getByPublicId(String publicId);

    List<LightweightStation> getLightweightBicycleStations();

    void dockBicycle(Bicycle bicycle);
}
