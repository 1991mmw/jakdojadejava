package JakDoJadeJavaDemo.service.exceptions;

public class BicycleDoesNotExist extends RuntimeException {

    public BicycleDoesNotExist(String publicId) {
        super("Bicycle with public id: " + publicId + " does not exist");
    }
}
