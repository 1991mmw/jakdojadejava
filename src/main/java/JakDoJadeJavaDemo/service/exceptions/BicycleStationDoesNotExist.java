package JakDoJadeJavaDemo.service.exceptions;

public class BicycleStationDoesNotExist extends RuntimeException {

    public BicycleStationDoesNotExist(String publicId) {
        super("Bicycle station with publicId: " + publicId + " does not exist.");
    }
}
