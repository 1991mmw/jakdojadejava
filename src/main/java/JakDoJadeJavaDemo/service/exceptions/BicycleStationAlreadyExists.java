package JakDoJadeJavaDemo.service.exceptions;

public class BicycleStationAlreadyExists extends RuntimeException {

    public BicycleStationAlreadyExists(String publicId) {
        super("Bicycle station with publicId: " + publicId + " already exists");
    }
}
