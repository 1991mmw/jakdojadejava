package JakDoJadeJavaDemo.service.impl;

import JakDoJadeJavaDemo.model.Bicycle;
import JakDoJadeJavaDemo.model.LightweightStation;
import JakDoJadeJavaDemo.model.Station;
import JakDoJadeJavaDemo.repository.BicycleRepository;
import JakDoJadeJavaDemo.repository.BicycleStationRepository;
import JakDoJadeJavaDemo.service.BicycleStationService;
import JakDoJadeJavaDemo.service.assembler.LightweightStationAssembler;
import JakDoJadeJavaDemo.service.exceptions.BicycleStationAlreadyExists;
import JakDoJadeJavaDemo.service.exceptions.BicycleStationDoesNotExist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BicycleStationServiceImpl implements BicycleStationService {

    @Autowired
    private BicycleStationRepository bicycleStationRepository;

    @Autowired
    private BicycleRepository bicycleRepository;

    @Autowired
    private LightweightStationAssembler assembler;

    @Override
    public void add(Station station) {
        //check if exists
        Station stationByPublicId = bicycleStationRepository.findByPublicId(station.getPublicId());
        if(stationByPublicId != null) {
            throw new BicycleStationAlreadyExists(station.getPublicId());
        }
        bicycleStationRepository.save(station);
    }

    @Override
    public void remove(String publicId) {
        //check if exists
        Station stationByPublicId = bicycleStationRepository.findByPublicId(publicId);
        if(stationByPublicId == null) {
            throw new BicycleStationDoesNotExist(publicId);
        } else {
            bicycleStationRepository.delete(stationByPublicId);
        }
    }

    @Override
    public void update(Station station) {
        //check if exists
        Station stationByPublicId = bicycleStationRepository.findByPublicId(station.getPublicId());
        if(stationByPublicId == null) {
            throw new BicycleStationDoesNotExist(station.getPublicId());
        }

        //update
        bicycleStationRepository.save(station);
    }

    @Override
    public Station getByPublicId(String publicId) {
        return bicycleStationRepository.findByPublicId(publicId);
    }

    @Override
    public List<LightweightStation> getLightweightBicycleStations() {
        List<Station> stationList = bicycleStationRepository.findAll();
        return stationList.stream()
                .map(station -> assembler.assembleLightweightStation(station))
                .collect(Collectors.toList());
    }

    @Override
    public void dockBicycle(Bicycle bicycle) {
        bicycleRepository.save(bicycle);
    }
}
