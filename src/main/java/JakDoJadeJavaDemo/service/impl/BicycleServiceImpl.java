package JakDoJadeJavaDemo.service.impl;

import JakDoJadeJavaDemo.model.Bicycle;
import JakDoJadeJavaDemo.repository.BicycleRepository;
import JakDoJadeJavaDemo.repository.BicycleStationRepository;
import JakDoJadeJavaDemo.service.BicycleService;
import JakDoJadeJavaDemo.service.exceptions.BicycleDoesNotExist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BicycleServiceImpl implements BicycleService {

    private final BicycleRepository bicycleRepository;

    private final BicycleStationRepository bicycleStationRepository;

    @Autowired
    public BicycleServiceImpl(BicycleRepository bicycleRepository, BicycleStationRepository bicycleStationRepository) {
        this.bicycleRepository = bicycleRepository;
        this.bicycleStationRepository = bicycleStationRepository;
    }

    @Override
    public void dockBicycle(String stationPublicId, String bicyclePublicId) {
        Bicycle bicycle = bicycleRepository.findByPublicId(bicyclePublicId);
        if(bicycle == null) {
            throw new BicycleDoesNotExist(bicyclePublicId);
        }
        bicycle.setStation(bicycleStationRepository.findByPublicId(stationPublicId));
        bicycle.setStatus("Docked");

        bicycleRepository.save(bicycle);
    }

    @Override
    public void rentBicycle(String stationPublicId, String bicyclePublicId) {
        Bicycle bicycle = bicycleRepository.findByPublicId(bicyclePublicId);
        if(bicycle == null) {
            throw new BicycleDoesNotExist(bicyclePublicId);
        }
        bicycle.setStation(null);
        bicycle.setStatus("In Use");
    }

    @Override
    public Bicycle getByPublicId(String publicId) {
        Bicycle bicycle = bicycleRepository.findByPublicId(publicId);
        if(bicycle == null) {
            throw new BicycleDoesNotExist(publicId);
        }
        return bicycle;
    }
}
