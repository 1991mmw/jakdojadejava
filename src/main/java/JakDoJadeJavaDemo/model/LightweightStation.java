package JakDoJadeJavaDemo.model;

public class LightweightStation {

    private String name;
    private String vacancies;
    private String occupiedRacks;
    private String availableBicycles;

    public LightweightStation() {
    }

    public LightweightStation(String name, String vacancies, String occupiedRacks, String availableBicycles) {
        this.name = name;
        this.vacancies = vacancies;
        this.occupiedRacks = occupiedRacks;
        this.availableBicycles = availableBicycles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVacancies() {
        return vacancies;
    }

    public void setVacancies(String vacancies) {
        this.vacancies = vacancies;
    }

    public String getOccupiedRacks() {
        return occupiedRacks;
    }

    public void setOccupiedRacks(String occupiedRacks) {
        this.occupiedRacks = occupiedRacks;
    }

    public String getAvailableBicycles() {
        return availableBicycles;
    }

    public void setAvailableBicycles(String availableBicycles) {
        this.availableBicycles = availableBicycles;
    }
}
