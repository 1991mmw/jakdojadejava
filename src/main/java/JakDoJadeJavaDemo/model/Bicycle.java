package JakDoJadeJavaDemo.model;

import javax.persistence.*;

@Entity(name = "bicycle")
public class Bicycle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "public_id")
    private String publicId;

    @ManyToOne
    @JoinColumn(name = "station_id")
    private Station station;

    @Column(name = "status")
    private String status;

    public Bicycle() {
    }

    public Bicycle(String publicId, Station station, String status) {
        this.publicId = publicId;
        this.station = station;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
