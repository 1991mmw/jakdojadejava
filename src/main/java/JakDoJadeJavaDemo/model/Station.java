package JakDoJadeJavaDemo.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "station")
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "public_id")
    private String publicId;

    @Column(name = "name")
    private String name;

    @Column(name = "total_bicycle_racks")
    private String totalBicycleRacks;

    @Column(name = "total_bicycle_units")
    private String totalBicycleUnits;

    @Column(name = "available_racks")
    private String availableRacks;

    @Column(name = "occupied_racks")
    private String occupiedRacks;

    @OneToMany(mappedBy = "station")
    private List<Bicycle> bicycles;

    public Station() {
    }

    public Station(String publicId, String name, String totalBicycleRacks, String totalBicycleUnits, String availableRacks, String occupiedRacks, List<Bicycle> bicycles) {
        this.publicId = publicId;
        this.name = name;
        this.totalBicycleRacks = totalBicycleRacks;
        this.totalBicycleUnits = totalBicycleUnits;
        this.availableRacks = availableRacks;
        this.occupiedRacks = occupiedRacks;
        this.bicycles = bicycles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalBicycleRacks() {
        return totalBicycleRacks;
    }

    public void setTotalBicycleRacks(String totalBicycleRacks) {
        this.totalBicycleRacks = totalBicycleRacks;
    }

    public String getTotalBicycleUnits() {
        return totalBicycleUnits;
    }

    public void setTotalBicycleUnits(String totalBicycleUnits) {
        this.totalBicycleUnits = totalBicycleUnits;
    }

    public String getAvailableRacks() {
        return availableRacks;
    }

    public void setAvailableRacks(String availableRacks) {
        this.availableRacks = availableRacks;
    }

    public String getOccupiedRacks() {
        return occupiedRacks;
    }

    public void setOccupiedRacks(String occupiedRacks) {
        this.occupiedRacks = occupiedRacks;
    }

    public List<Bicycle> getBicycles() {
        return bicycles;
    }

    public void setBicycles(List<Bicycle> bicycles) {
        this.bicycles = bicycles;
    }
}
