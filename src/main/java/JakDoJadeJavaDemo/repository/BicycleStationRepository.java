package JakDoJadeJavaDemo.repository;

import JakDoJadeJavaDemo.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Transactional
@Repository
public interface BicycleStationRepository extends JpaRepository<Station, Long> {

    Station findByPublicId(String publicId);
}
