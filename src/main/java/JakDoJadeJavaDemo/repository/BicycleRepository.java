package JakDoJadeJavaDemo.repository;

import JakDoJadeJavaDemo.model.Bicycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface BicycleRepository extends JpaRepository<Bicycle, Long> {

    Bicycle findByPublicId(String publicId);
}
