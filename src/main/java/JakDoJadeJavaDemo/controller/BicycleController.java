package JakDoJadeJavaDemo.controller;

import JakDoJadeJavaDemo.model.Bicycle;
import JakDoJadeJavaDemo.service.BicycleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/bicycle")
public class BicycleController {

    private final BicycleService service;

    @Autowired
    public BicycleController(BicycleService service) {
        this.service = service;
    }

    @RequestMapping(value = "/dock/{bicyclePublicId}/{stationPublicId}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity dockBicycleIntoStation(@PathVariable (value = "bicyclePublicId") String bicyclePublicId,
                                       @PathVariable (value = "stationPublicId") String stationPublicId) {
        service.dockBicycle(stationPublicId, bicyclePublicId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/rent/{bicyclePublicId}/{stationPublicId}", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity rentBicycleIntoStation(@PathVariable (value = "bicyclePublicId") String bicyclePublicId,
                                                 @PathVariable (value = "stationPublicId") String stationPublicId) {
        service.rentBicycle(stationPublicId, bicyclePublicId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/getByPublicId", method = RequestMethod.GET, produces = "application/json")
    public Bicycle getBicycleByPublicId(@RequestParam(value = "publicId") String publicId) {
        return service.getByPublicId(publicId);
    }

}
