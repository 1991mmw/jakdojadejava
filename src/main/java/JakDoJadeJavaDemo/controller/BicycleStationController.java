package JakDoJadeJavaDemo.controller;

import JakDoJadeJavaDemo.model.LightweightStation;
import JakDoJadeJavaDemo.model.Station;
import JakDoJadeJavaDemo.service.BicycleStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/station")
public class BicycleStationController {

    private final BicycleStationService service;

    @Autowired
    public BicycleStationController(BicycleStationService service) {
        this.service = service;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addBicycleStation(@RequestBody Station station) {
        service.add(station);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public ResponseEntity removeBicycleStation(@RequestParam(value = "publicId") String publicId) {
        service.remove(publicId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public ResponseEntity updateBicycleStation(@RequestBody Station station) {
        service.update(station);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/getByPublicId", method = RequestMethod.GET, produces = "application/json")
    public Station getBicycleStationByPublicId(@RequestParam(value = "publicId") String publicId) {
        return service.getByPublicId(publicId);
    }

    @RequestMapping(value = "/getLightweightBicycleStations", method = RequestMethod.GET, produces = "application/json")
    public List<LightweightStation> getLightweightBicycleStations() {
        return service.getLightweightBicycleStations();
    }

}
